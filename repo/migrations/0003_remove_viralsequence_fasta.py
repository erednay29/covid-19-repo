# Generated by Django 3.1.7 on 2021-04-18 10:20

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('repo', '0002_auto_20210414_1334'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='viralsequence',
            name='fasta',
        ),
    ]
