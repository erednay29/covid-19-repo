from django.utils import timezone
from .models import Collection, ViralSequence
from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User
from django.contrib.messages import get_messages

# Create your tests here.


class LoginTests(TestCase):

    def setUp(self):
        self.credentials = {
            'username': 'TestUser',
            'password': 'secret'
        }
        User.objects.create_user(**self.credentials)

    def test_signin(self):
        response = self.client.post(
            reverse('repo:login'), self.credentials, follow=True)

        self.assertTrue(response.context['user'].is_active)
        self.assertRedirects(response, reverse(
            'repo:home'), target_status_code=200)

    def test_logout(self):
        loggedin = self.client.login(**self.credentials)
        self.assertTrue(loggedin)

        response = self.client.get(reverse('repo:logout'), follow=True)
        self.assertFalse(response.context['user'].is_active)
        self.assertRedirects(response, reverse(
            'repo:home'), target_status_code=200)

    def test_wrong_password(self):
        response = self.client.post(reverse('repo:login'), {
                                    'username': self.credentials['username'], 'password': 'incorrectPassword'})

        self.assertFalse(response.context['user'].is_active)


class CollectionTests(TestCase):

    def setUp(self):
        self.credentials = {
            'username': 'TestUser',
            'password': 'secret'
        }
        User.objects.create_user(**self.credentials)
        ViralSequence.objects.create(
            accession="Acc1", collection_date=timezone.now())
        ViralSequence.objects.create(
            accession="Acc2", collection_date=timezone.now())

    def test_create_collection(self):
        loggedin = self.client.login(**self.credentials)
        self.assertTrue(loggedin)

        response = self.client.post(reverse('repo:collections_create'),
                                    data={'collection_name': 'Test', 'hidden_choice': '', 'create': 'create'})

        collection = Collection.objects.get(name='Test')
        self.assertRedirects(response, reverse(
            'repo:collection_details', args=(collection.pk,)), target_status_code=200)

    def test_create_collection_no_name(self):
        loggedin = self.client.login(**self.credentials)
        self.assertTrue(loggedin)

        response = self.client.post(reverse('repo:collections_create'),
                                    data={'collection_name': '', 'hidden_choice': '', 'create': 'create'})

        messages = list(get_messages(response.wsgi_request))
        self.assertEqual(len(messages), 1)
        self.assertEqual(
            str(messages[0]), 'You have to name your collection before submiting')

    def test_create_collection_with_sequences(self):
        loggedin = self.client.login(**self.credentials)
        self.assertTrue(loggedin)
        sequences = str(ViralSequence.objects.get(accession='Acc1').pk) + \
            ',' + str(ViralSequence.objects.get(accession='Acc2').pk)

        response = self.client.post(reverse('repo:collections_create'),
                                    data={'collection_name': 'Test', 'hidden_choice': sequences, 'create': 'create'})

        collection = Collection.objects.get(name='Test')
        self.assertEqual(len(collection.sequences.all()), 2)
        self.assertRedirects(response, reverse(
            'repo:collection_details', args=(collection.pk,)), target_status_code=200)


class CollectionEditTests(TestCase):

    def setUp(self):
        self.credentials = {
            'username': 'TestUser',
            'password': 'secret'
        }
        user = User.objects.create_user(**self.credentials)
        sequences = []
        sequences.append(ViralSequence.objects.create(
            accession="Acc1", collection_date=timezone.now()))
        sequences.append(ViralSequence.objects.create(
            accession="Acc2", collection_date=timezone.now()))
        collection = Collection.objects.create(name="Test", owner=user)
        collection.sequences.add(*sequences)

    def test_add_new_sequence(self):
        loggedin = self.client.login(**self.credentials)
        self.assertTrue(loggedin)

        collection = Collection.objects.get(name='Test')
        begin_seq = len(collection.sequences.all())
        new_seq = ViralSequence.objects.create(
            accession="Acc3", collection_date=timezone.now())

        response = self.client.post(reverse('repo:add_to_collection', args=(
            collection.pk,)), data={'hidden_choice': str(new_seq.pk), 'add': 'add'})

        self.assertRedirects(response, reverse(
            'repo:collection_details', args=(collection.pk,)), target_status_code=200)
        self.assertEqual(begin_seq + 1, len(collection.sequences.all()))

    def test_add_zero_sequences(self):
        loggedin = self.client.login(**self.credentials)
        self.assertTrue(loggedin)

        collection = Collection.objects.get(name='Test')
        begin_seq = len(collection.sequences.all())

        response = self.client.post(reverse('repo:add_to_collection', args=(
            collection.pk,)), data={'hidden_choice': '', 'add': 'add'})

        self.assertRedirects(response, reverse(
            'repo:collection_details', args=(collection.pk,)), target_status_code=200)
        self.assertEqual(begin_seq, len(collection.sequences.all()))

    def test_remove_sequence(self):
        loggedin = self.client.login(**self.credentials)
        self.assertTrue(loggedin)

        collection = Collection.objects.get(name='Test')
        begin_seq = len(collection.sequences.all())
        sequence = collection.sequences.all()[0]

        response = self.client.get(
            reverse('repo:collection_sequence_delete', args=(collection.pk, sequence.pk, )))

        self.assertContains(response, 'Are you sure you want to delete "' +
                            sequence.accession + '" from "' + collection.name + '"?')

        response = self.client.post(
            reverse('repo:collection_sequence_delete', args=(collection.pk, sequence.pk, )))

        self.assertEqual(begin_seq - 1, len(collection.sequences.all()))
        self.assertRedirects(response, reverse(
            'repo:collection_details', args=(collection.pk,)))

    def test_remove_collection(self):
        loggedin = self.client.login(**self.credentials)
        self.assertTrue(loggedin)

        collection = Collection.objects.get(name='Test')

        response = self.client.get(
            reverse('repo:collection_delete', args=(collection.pk, )))

        self.assertContains(
            response, 'Are you sure you want to delete "' + collection.name + '"?')

        response = self.client.post(
            reverse('repo:collection_delete', args=(collection.pk, )))

        self.assertRedirects(response, reverse('repo:collections'))

        response = self.client.get(
            reverse('repo:collection_details', args=(collection.pk, )))
        self.assertEqual(404, response.status_code)

    def test_change_collection_name(self):
        loggedin = self.client.login(**self.credentials)
        self.assertTrue(loggedin)
        collection = Collection.objects.get(name='Test')

        response = self.client.post(reverse('repo:collection_details', args=(
            collection.pk, )), data={'name': 'NewTest'})
        self.assertEqual(200, response.status_code)

        collection = Collection.objects.get(pk=collection.pk)
        self.assertEqual(collection.name, 'NewTest')

    def test_change_collection_empty_name(self):
        loggedin = self.client.login(**self.credentials)
        self.assertTrue(loggedin)
        collection = Collection.objects.get(name='Test')

        response = self.client.post(
            reverse('repo:collection_details', args=(collection.pk, )), data={'name': ''})
        self.assertEqual(200, response.status_code)

        collection = Collection.objects.get(pk=collection.pk)
        self.assertEqual(collection.name, 'Test')


class AuthTests(TestCase):

    def setUp(self):
        self.credentials = {
            'username': 'TestUser',
            'password': 'secret'
        }
        user = User.objects.create_user(**self.credentials)
        sequences = []
        sequences.append(ViralSequence.objects.create(
            accession="Acc1", collection_date=timezone.now()))
        sequences.append(ViralSequence.objects.create(
            accession="Acc2", collection_date=timezone.now()))
        collection = Collection.objects.create(name="Test", owner=user)
        collection.sequences.add(*sequences)

    def test_detail_anonymous(self):
        collection = Collection.objects.get(name='Test')

        response = self.client.get(
            reverse('repo:collection_details', args=(collection.pk, )), follow=True)
        self.assertRedirects(response, reverse('repo:login') + '?next=/collections/' +
                             str(collection.pk) + '/details')
        self.assertContains(response, 'Please login to see this page.')

    def test_detail_wrong_user(self):
        credentials = {
            'username': 'TestUser2',
            'password': 'secret'
        }
        User.objects.create_user(**credentials)
        self.client.login(**credentials)
        collection = Collection.objects.get(name='Test')

        response = self.client.get(
            reverse('repo:collection_details', args=(collection.pk, )), follow=True)
        self.assertRedirects(response, reverse('repo:login') + '?next=/collections/' +
                             str(collection.pk) + '/details')
        self.assertContains(
            response, 'Your account doesn\'t have access to this page. To proceed, please login with an account that has access.')

    def test_try_delete_collection_anonymous(self):
        collection = Collection.objects.get(name='Test')

        response = self.client.post(
            reverse('repo:collection_delete', args=(collection.pk, )), follow=True)
        self.assertRedirects(response, reverse('repo:login') + '?next=/collections/' +
                             str(collection.pk) + '/delete')
        self.assertContains(response, 'Please login to see this page.')

        self.client.login(**self.credentials)
        response = self.client.get(
            reverse('repo:collection_details', args=(collection.pk, )))
        self.assertEqual(200, response.status_code)
        self.assertContains(response, collection.name)

    def test_try_delete_collection_wrong_user(self):
        credentials = {
            'username': 'TestUser2',
            'password': 'secret'
        }
        User.objects.create_user(**credentials)
        self.client.login(**credentials)
        collection = Collection.objects.get(name='Test')

        response = self.client.post(
            reverse('repo:collection_delete', args=(collection.pk, )), follow=True)
        self.assertRedirects(response, reverse('repo:login') + '?next=/collections/' +
                             str(collection.pk) + '/delete')
        self.assertContains(
            response, 'Your account doesn\'t have access to this page. To proceed, please login with an account that has access.')

        self.client.login(**self.credentials)
        response = self.client.get(
            reverse('repo:collection_details', args=(collection.pk, )))
        self.assertEqual(200, response.status_code)
        self.assertContains(response, collection.name)

    def test_try_delete_collection_sequence_anonymous(self):
        collection = Collection.objects.get(name='Test')
        sequence = collection.sequences.all()[0]

        response = self.client.post(reverse('repo:collection_sequence_delete', args=(
            collection.pk, sequence.pk, )), follow=True)
        self.assertRedirects(response, reverse('repo:login') + '?next=/collections/' +
                             str(collection.pk) + '/' + str(sequence.pk) + '/delete')
        self.assertContains(response, 'Please login to see this page.')

        self.client.login(**self.credentials)
        response = self.client.get(
            reverse('repo:collection_details', args=(collection.pk, )))
        self.assertEqual(200, response.status_code)
        self.assertContains(response, collection.name)
        self.assertContains(response, sequence.accession)

    def test_try_delete_collection_sequence_wrong_user(self):
        credentials = {
            'username': 'TestUser2',
            'password': 'secret'
        }
        User.objects.create_user(**credentials)
        self.client.login(**credentials)

        collection = Collection.objects.get(name='Test')
        sequence = collection.sequences.all()[0]

        response = self.client.post(reverse('repo:collection_sequence_delete', args=(
            collection.pk, sequence.pk, )), follow=True)
        self.assertRedirects(response, reverse('repo:login') + '?next=/collections/' +
                             str(collection.pk) + '/' + str(sequence.pk) + '/delete')
        self.assertContains(
            response, 'Your account doesn\'t have access to this page. To proceed, please login with an account that has access.')

        self.client.login(**self.credentials)
        response = self.client.get(
            reverse('repo:collection_details', args=(collection.pk, )))
        self.assertEqual(200, response.status_code)
        self.assertContains(response, collection.name)
        self.assertContains(response, sequence.accession)

    def test_try_download_pdf_anonymous(self):
        collection = Collection.objects.get(name='Test')

        response = self.client.get(
            reverse('repo:download_pdf', args=(collection.pk, )), follow=True)
        self.assertRedirects(response, reverse('repo:login') + '?next=/collections/' +
                             str(collection.pk) + '/download_pdf')
        self.assertContains(response, 'Please login to see this page.')

    def test_try_download_pdf_wrong_user(self):
        credentials = {
            'username': 'TestUser2',
            'password': 'secret'
        }
        User.objects.create_user(**credentials)
        self.client.login(**credentials)

        collection = Collection.objects.get(name='Test')

        response = self.client.get(
            reverse('repo:download_pdf', args=(collection.pk, )), follow=True)
        self.assertRedirects(response, reverse('repo:login') + '?next=/collections/' +
                             str(collection.pk) + '/download_pdf')
        self.assertContains(
            response, 'Your account doesn\'t have access to this page. To proceed, please login with an account that has access.')
