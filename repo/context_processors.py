
from .models import Contact


def footer(self):
    contact, _ = Contact.objects.get_or_create(name='site_contact', defaults={
                                            'email': 'some_email@gmail.com',
                                            'phone_number': '+48 666-666-666', 
                                            'address': 'Some city, Some street 69'})
    
    return {
        'EMAIL': contact.email,
        'PHONE_NUMBER': contact.phone_number,
        'ADDRESS': contact.address
    }
