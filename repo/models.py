from django.db import models
from django.db.models.fields.related import ForeignKey, ManyToManyField
from django.contrib.auth.models import User
from django.utils import timezone

# Create your models here.

class Taxonomy(models.Model):
    name = models.CharField('name', max_length=200)
    value = models.CharField('value', max_length=200)
    count = models.IntegerField('count')

    def __str__(self):
        return self.name

class Center(models.Model):
    name = models.CharField('name', max_length=200)
    value = models.CharField('value', max_length=200)
    count = models.IntegerField('count')

    def __str__(self):
        return self.name

class Country(models.Model):
    name = models.CharField('name', max_length=200)
    value = models.CharField('value', max_length=200)
    count = models.IntegerField('count')

    def __str__(self):
        return self.name

class ViralSequence(models.Model):
    accession = models.CharField('accession', max_length=200, unique=True)
    center = models.ForeignKey(Center, null=True, on_delete=models.DO_NOTHING)
    country = models.ForeignKey(Country, null=True, on_delete=models.DO_NOTHING)
    taxonomy = models.ForeignKey(Taxonomy, null=True, on_delete=models.DO_NOTHING)
    collection_date = models.DateField('collection date')    
    host = models.CharField('host', max_length=200)
    isolate = models.CharField('isolate', max_length=200)

    def __str__(self):
        return self.accession

class Collection(models.Model):
    owner = ForeignKey(User, on_delete=models.DO_NOTHING)
    sequences = ManyToManyField(ViralSequence)
    name = models.CharField('name', max_length=200)
    creation_date = models.DateTimeField('creation_date', default=timezone.now)

    def __str__(self):
        return self.name

class Contact(models.Model):
    name = models.CharField('name', max_length=200)
    email = models.EmailField('email', max_length=200)
    phone_number = models.CharField('phone_number', max_length=200)
    address = models.CharField('address', max_length=200)

    def __str__(self):
        return self.name