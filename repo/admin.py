from django.contrib import admin, messages
from django.http.response import HttpResponse, HttpResponseRedirect
from django.urls import path, reverse
from django.core.exceptions import ObjectDoesNotExist
import requests
import datetime as dt
import math

from .models import ViralSequence, Taxonomy, Country, Center, Collection, Contact

# Register your models here.
admin.site.register(Taxonomy)
admin.site.register(Country)
admin.site.register(Center)
admin.site.register(Collection)
admin.site.register(Contact)

@admin.register(ViralSequence)
class ViralSequenceAdmin(admin.ModelAdmin):
    change_list_template = 'admin/repo/viralsequence/change_list.html'

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('refresh/', self.refresh, name='refresh')
        ]
        return my_urls + urls

    def get_object_or_null(self, model, str):
        try:
            obj = model.objects.get(value=str)
        except ObjectDoesNotExist:
            obj = None
        return obj

    def format_date(self, date):
        if int(date) % 100 != 0:
            return ('%Y%m%d', date)
        elif int(date) % 10000 != 0:
            return ('%Y%m', date[:-2])
        else:
            return ('%Y', date[:-4])

    def refresh(self, request):
        count = 0
        try:
            r = requests.get(
                'https://www.covid19dataportal.org/api/backend/viral-sequences/sequences?page=1&size=0')
            r.raise_for_status()

            count = r.json()['hitCount']
            if ViralSequence.objects.count() >= count:
                messages.info(request, 'Database already up to date')
                return HttpResponseRedirect(reverse('admin:repo_viralsequence_changelist'))

            for facet in r.json()['facets']:
                id = facet['id']
                if id == 'center_name':
                    model = Center
                elif id == 'TAXONOMY':
                    model = Taxonomy
                elif id == 'country':
                    model = Country
                else:
                    continue

                for values in facet['facetValues']:
                    obj, created = model.objects.update_or_create(
                        name=values['label'], value=values['value'],
                        defaults={'count': values['count']}
                    )

        except requests.exceptions.HTTPError as e:
            messages.error(request, 'Failed to fetch remote database: ' + e.response.text)
            return HttpResponseRedirect(reverse('admin:repo_viralsequence_changelist'))

        page_count = math.ceil(count / 1000)
        starting_page = count - ViralSequence.objects.count()
        starting_page = page_count - math.floor(starting_page / 1000) - 1
        page = starting_page
        while page <= page_count:
            try:
                r = requests.get(
                    'https://www.covid19dataportal.org/api/backend/viral-sequences/sequences?page=' + str(page) + '&size=1000')
                r.raise_for_status()

                for entry in r.json()['entries']:
                    fields = entry['fields']
                    if fields['collection_date']:
                        dateformat, date = self.format_date(
                            fields['collection_date'][0])
                    else:
                        dateformat = ''
                        date = ''

                    obj, created = ViralSequence.objects.update_or_create(
                        accession=entry['id'],
                        defaults={
                            'center': self.get_object_or_null(
                                Center, fields['center_name'][0] if fields['center_name'] else ''),
                            'country': self.get_object_or_null(
                                Country, fields['country'][0] if fields['country'] else ''),
                            'taxonomy': self.get_object_or_null(
                                Taxonomy, fields['TAXON'][0] if fields['TAXON'] else ''),
                            'collection_date': dt.datetime.strptime(
                                date, dateformat),
                            'host': fields['host'][0] if fields['host'] else '',
                            'isolate': fields['isolate'][0] if fields['isolate'] else ''
                        }
                    )

                page += 1
            except requests.exceptions.HTTPError as e:
                messages.error(request, 'Failed to fetch remote database: ' + e.response.text)
                return HttpResponseRedirect(reverse('admin:repo_viralsequence_changelist'))

        messages.info(request, 'Succesfully updated database')
        return HttpResponseRedirect(reverse('admin:repo_viralsequence_changelist'))
