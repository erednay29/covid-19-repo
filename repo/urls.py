from django.conf.urls import include
from django.urls import path
from django.contrib.auth import views as auth_views
import django.contrib.auth.urls

from . import views

app_name = 'repo'
urlpatterns = [
    path('', views.IndexView.as_view(), name='home'),
    path('database', views.DatabaseView.as_view(), name='database'),
    path('collections', views.CollectionsView.as_view(), name='collections'),
    path('collections/create', views.create_collection, name='collections_create'),
    path('collections/<int:pk>/details',
         views.CollectionDetailsView.as_view(), name='collection_details'),
    path('collections/<int:pk>/delete',
         views.CollectionDeleteView.as_view(), name='collection_delete'),
    path('collections/<int:collection_id>/<int:sequence_id>/delete',
         views.collection_sequence_delete, name='collection_sequence_delete'),
    path('collections/<int:collection_id>/add',
         views.add_to_collection, name='add_to_collection'),
    path('collections/<int:collection_id>/download_pdf',
         views.download_collection_pdf, name='download_pdf'),
    path('stats', views.stats, name='stats'),

    path('accounts/', include('django.contrib.auth.urls'))
]
