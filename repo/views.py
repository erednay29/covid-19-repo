import time
from django.contrib import messages
from django.contrib.auth.views import redirect_to_login
from django.core.files.storage import FileSystemStorage
from django.http.response import FileResponse, HttpResponse, HttpResponseRedirect
from django.urls.base import reverse, reverse_lazy
from django.shortcuts import get_object_or_404, render
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.core.paginator import Paginator
from django.contrib.auth.decorators import login_required
from django.template.loader import render_to_string
from weasyprint import HTML
import json

from .models import Center, ViralSequence, Collection

# Helper functions
from .views_helpers import *


class IndexView(generic.ListView):
    model = ViralSequence
    template_name = 'repo/home.html'
    context_object_name = "sequences"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_obj'] = ViralSequence.objects.order_by(
            '-collection_date')[:5]
        if self.request.user.is_authenticated:
            context['collections'] = Collection.objects.filter(
                owner=self.request.user).order_by('-creation_date')[:5]
        return context


class DatabaseView(generic.ListView):
    paginate_by = 20
    model = ViralSequence
    template_name = 'repo/database.html'

    def get_queryset(self):
        return get_table_queryset(self.request)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return get_table_context_data(context)


class CollectionsView(LoginRequiredMixin, generic.ListView):
    model = Collection
    template_name = 'repo/collections.html'
    context_object_name = 'collections'

    login_url = '/accounts/login/'

    def get_queryset(self):
        user = self.request.user
        return Collection.objects.filter(owner=user)


@login_required()
def create_collection(request):
    sequences = get_table_queryset(request)
    context = get_paginated_context(request, sequences)

    if request.method == 'POST':
        if 'create' in request.POST:
            if 'collection_name' in request.POST and request.POST['collection_name'].strip() != '':
                collection = Collection()
                collection.owner = request.user
                collection.name = request.POST.get('collection_name')
                collection.save()
                if (request.POST.get('hidden_choice') != '' and request.POST.get('hidden_choice') != None):
                    choices = request.POST.get('hidden_choice')
                    choices = [int(choice) for choice in choices.split(',')]
                    collection.sequences.add(*choices)
                return HttpResponseRedirect(reverse('repo:collection_details', args=(collection.pk,)))
            else:
                messages.add_message(
                    request, messages.ERROR, 'You have to name your collection before submiting')
                return render(request, 'repo/collections/create.html', context)

    return render(request, 'repo/collections/create.html', context)


@login_required()
def add_to_collection(request, collection_id):
    collection = get_object_or_404(Collection, pk=collection_id)

    sequences = get_table_queryset(request)
    col_seq = collection.sequences.all()
    sequences = sequences.difference(col_seq)
    context = get_paginated_context(request, sequences)

    if request.method == 'POST':
        if 'add' in request.POST:
            if (request.POST.get('hidden_choice') != '' and request.POST.get('hidden_choice') != None):
                choices = request.POST.get('hidden_choice')
                choices = [int(choice) for choice in choices.split(',')]
                collection.sequences.add(*choices)
            return HttpResponseRedirect(reverse('repo:collection_details', args=(collection.pk,)))

    return render(request, 'repo/collections/add.html', context)


class CollectionDetailsView(LoginRequiredMixin, UserPassesTestMixin, generic.DetailView):
    model = Collection
    template_name = 'repo/collections/details.html'
    context_object_name = 'collection'

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        name = request.POST.get('name')
        if name:
            self.object.name = name
            self.object.save()

        return self.render_to_response(self.get_context_data(**kwargs))

    def get_context_data(self, **kwargs):
        context = super(CollectionDetailsView, self).get_context_data(**kwargs)
        context = get_table_context_data(context)
        sequences = self.get_object().sequences.all()
        order, filters = get_table_filters(self.request)
        filtered = sequences.filter(**filters).order_by(order)
        paginator = Paginator(filtered, 20)
        page = self.request.GET.get('page')
        context['page_obj'] = paginator.get_page(page)
        return context

    def test_func(self):
        return self.request.user == self.get_object().owner

    def handle_no_permission(self):
        return redirect_to_login(self.request.GET.get('next', self.request.POST.get('next', reverse('repo:collection_details', args=(self.get_object().pk,)))))


class CollectionDeleteView(LoginRequiredMixin, UserPassesTestMixin, generic.DeleteView):
    model = Collection
    success_url = reverse_lazy('repo:collections')
    template_name = 'repo/confirm_delete.html'

    def test_func(self):
        return self.request.user == self.get_object().owner

    def handle_no_permission(self):
        return redirect_to_login(self.request.GET.get('next', self.request.POST.get('next', reverse('repo:collection_delete', args=(self.get_object().pk,)))))


@login_required()
def collection_sequence_delete(request, collection_id, sequence_id):
    sequence = get_object_or_404(ViralSequence, pk=sequence_id)
    collection = get_object_or_404(Collection, pk=collection_id)

    if request.user != collection.owner:
        return redirect_to_login(request.GET.get('next', request.POST.get('next', reverse('repo:collection_sequence_delete', args=(collection.pk, sequence.pk)))))

    if request.method == 'POST':
        collection.sequences.remove(sequence)
        return HttpResponseRedirect(reverse('repo:collection_details', args=(collection_id,)))

    return render(request, 'repo/confirm_delete.html', context={'object': sequence, 'seq_collection': collection})


@login_required()
def download_collection_pdf(request, collection_id):
    collection = get_object_or_404(Collection, pk=collection_id)

    if request.user != collection.owner:
        return redirect_to_login(request.GET.get('next', request.POST.get('next', reverse('repo:download_pdf', args=(collection.pk, )))))

    html_string = render_to_string(
        'repo/collections/pdf.html', {'page_obj': collection.sequences.all()})

    html = HTML(string=html_string)
    html.write_pdf(target='/tmp/' + str(collection.name) + '.pdf')

    fs = FileSystemStorage('/tmp')
    with fs.open(str(collection.name) + '.pdf') as pdf:
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="' + \
            str(collection.name) + '.pdf"'
        return response

    return HttpResponseRedirect(reverse('repo:collection_details', args=(collection_id,)))


def stats(request):
    country_names = []
    country_count = []
    center_names = []
    center_count = []

    for country in Country.objects.all():
        country_names.append(country.name)
        country_count.append(country.count)

    for center in Center.objects.all():
        center_names.append(center.name)
        center_count.append(center.count)

    context = {
        'country_names': json.dumps(country_names),
        'country_count': json.dumps(country_count),
        'center_names': json.dumps(center_names),
        'center_count': json.dumps(center_count)
    }

    return render(request, 'repo/stats.html', context)
