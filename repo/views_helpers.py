from django.core.paginator import Paginator
from django.db.models.query import QuerySet

from .models import ViralSequence, Country, Collection


def get_table_filters(request):
    order = request.GET.get('orderby', 'pk')
    if order == '':
        order = 'pk'
    d = {}
    year = request.GET.get('year', None)
    country = request.GET.get('country', None)
    accession = request.GET.get('accession', None)

    if year != None and year != '':
        d["collection_date__year"] = year
    if country != None and country != '':
        d["country__name"] = country
    if accession != None and accession != '':
        d["accession__contains"] = accession

    return (order, d)


def get_table_queryset(request):
    order, d = get_table_filters(request)

    return ViralSequence.objects.filter(**d).order_by(order)


def get_table_context_data(context):
    context["countries"] = Country.objects.all()
    context["years"] = ViralSequence.objects.dates('collection_date', 'year')
    return context


def get_paginated_context(request, sequences):
    paginator = Paginator(sequences, 20)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context = {'page_obj': page_obj}
    return get_table_context_data(context)
